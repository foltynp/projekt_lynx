﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Lynx.Class;

namespace Lynx.Class
{


    class Symulation
    {
        ComboBox listOfElement;
        Button btn_AddEle,btn_RmvEle;
        ListBox listOfEle;
        TextBox txtBox_current, txtBox_resistance, txtEle_intensity, txtBox_eleName;
        List <CurrentGen> listaBateri = new List<CurrentGen>();
        List <Resistor> listaRezystorow = new List<Resistor>();
        public Symulation(ComboBox cmbx_ListOfElement, Button btn_AddEle, ListBox listOfEle, Button btn_remove, TextBox txtBox_current, TextBox txtBox_eleName, TextBox txtEle_intensity, TextBox txtBox_resistance)
        {
            this.listOfElement = cmbx_ListOfElement;
            this.btn_AddEle = btn_AddEle;
            this.listOfEle = listOfEle;
            this.btn_RmvEle = btn_remove;
            this.txtBox_current = txtBox_current;
            this.txtBox_resistance = txtBox_resistance;
            this.txtEle_intensity = txtEle_intensity;
            this.txtBox_eleName = txtBox_eleName;


            listOfElement.Items.Add("Rezystor");
            listOfElement.Items.Add("Bateria");

            btn_AddEle.Click += Btn_AddEle_Click;
            btn_RmvEle.Click += Btn_RmvEle_Click;        }

        private void Btn_RmvEle_Click(object sender, RoutedEventArgs e)
        {
            listOfEle.Items.Remove(listOfEle.SelectedItem);
        }

        private void Btn_AddEle_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string nazwaElementu = listOfElement.Text;
            if (String.IsNullOrEmpty(nazwaElementu))
                { MessageBox.Show("Wybierz prosze najpierw element"); }
            else
            {
                //dodaj elemnt
                bool prase;
                double rezystancja, napiecie, natezenie;
                //stwórzlistę klas
                switch (nazwaElementu)
                {
                    case "Rezystor":

                        prase = double.TryParse(txtBox_resistance.Text, out rezystancja);
                        if (prase && !string.IsNullOrEmpty(txtBox_eleName.Text))
                        {
                            listaRezystorow.Add(new Resistor(rezystancja, txtBox_eleName.Text));
                            MessageBox.Show("Opór: " + rezystancja);
                            listOfEle.Items.Add(txtBox_eleName.Text);
                        }
                        else
                            MessageBox.Show("Błąd danych");
                            break;
                    case "Bateria":
                        prase = double.TryParse(txtBox_resistance.Text, out rezystancja);
                        if (!prase)
                            rezystancja = 0;
                        prase = double.TryParse(txtBox_current.Text, out napiecie);
                        if (!prase)
                            rezystancja = 0;
                        prase = double.TryParse(txtEle_intensity.Text, out natezenie);
                        if (!prase)
                            rezystancja = 0;

                        if (prase && !string.IsNullOrEmpty(txtBox_eleName.Text))
                        {
                            listaBateri.Add(new CurrentGen(napiecie, natezenie, txtBox_eleName.Text));
                            MessageBox.Show("Opór: " + rezystancja+" napięcie: "+ napiecie);
                            listOfEle.Items.Add(txtBox_eleName.Text);
                        }
                        else
                            MessageBox.Show("Błąd danych");
                        break;
                    default:
                        MessageBox.Show("Element nie znaleziony");
                        break;
                }
            }
        }
      
        public void Oblicz(string option) {
            //musimy przeiterować po obu listach i pobliczać wg. wzoru
            double maxCurrent = 0;
            double maxIntesity = 0;
            double maxResistance = 0;
            switch (option)
            {
                case "current":
                    
                    foreach (var el in listaBateri)
                    {
                        maxCurrent += el.eleFlow.GetCurrent();
                    }
                    MessageBox.Show("Maksymalne napięcie wynosi: " + maxCurrent);
                    break;
                case "intesity":
                    foreach (var el in listaBateri)
                    {
                        maxIntesity += el.eleFlow.GetIntesity();
                    }
                    MessageBox.Show("Maksymalne natężenie wynosi: " + maxIntesity);
                    break;

                case "resistance":
                    foreach (var el in listaBateri)
                    {
                        maxResistance += el.eleFlow.GetResistance();
                    }

                    foreach (var el in listaRezystorow)
                    {
                        maxResistance += el.eleFlow.GetResistance();
                    }
                    MessageBox.Show("Maksymalny opór wynosi: " + maxResistance);
                    break;
                default:
                    break;
            }

        }
       
    }
}
