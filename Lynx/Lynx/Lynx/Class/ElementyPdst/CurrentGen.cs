﻿using Lynx.Class.Rezystor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.Class
{
    class CurrentGen : ElementBluePrint
    {
        #region Zmienne
        private bool isGenerator = false;

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="intesity"></param>
        /// <param name="isGenerator"></param>
        public CurrentGen(double current,double intesity, bool isGenerator, string nazwa)
        {            
            if (isGenerator) { this.isGenerator = isGenerator; };
            eleFlow = new EleFlow(current, 0, intesity);
            this.nazwaElementu = nazwa;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="intesity"></param>
        public CurrentGen(double current, double intesity, string nazwa)
        {
            eleFlow = new EleFlow(current, 0, intesity);
            this.nazwaElementu = nazwa;
        }

        

    }
}
