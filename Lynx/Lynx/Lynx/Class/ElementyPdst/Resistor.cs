﻿using Lynx.Class.Rezystor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.Class
{
    class Resistor : ElementBluePrint
    {
        public Resistor(double resitance, string name)
        {
            this.nazwaElementu = name;
            eleFlow = new EleFlow(0, resitance, 0);
        }
    }
}
