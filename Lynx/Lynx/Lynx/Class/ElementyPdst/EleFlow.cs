﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.Class.Rezystor
{
    class EleFlow
    {
        #region
        /// <summary>
        /// Klasa ta istnieje tylko po to aby odzwierciedlić iż wszystkie obiekty musza mieć w swoim punkcie natężenie/napięcie/opór
        /// </summary>
        #endregion


        private double current;
        private double resistance;
        private double intensity;
        private int frequency;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="resistance"></param>
        /// <param name="intensity"></param>
        public EleFlow(double current, double resistance, double intensity)
        {
            this.current = current;
            this.resistance = resistance;
            this.intensity = intensity;
        }

        public double GetCurrent() => current;
        public double GetResistance() => resistance;
        public double GetIntesity() => intensity;

        public int GetFrequency() => frequency;
        public void SetFrequency(int frequency) => this.frequency = frequency;


    }       
}
