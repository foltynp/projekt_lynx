﻿using Lynx.Class;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lynx
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Symulation symulation;
        public MainWindow()
        {
            InitializeComponent();
            symulation = new Symulation(
                cmbx_ListOfElements,
                btn_AddEle,ListOfEle,
                btn_remove,
                txtBox_current,
                txtBox_eleName,
                txtEle_intensity,
                txtBox_resistance);


        }

        private void Btn_okPar_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)curentOption.IsChecked)
                symulation.Oblicz("current");
            if ((bool)intesityOption.IsChecked)
                symulation.Oblicz("intesity");
            if ((bool)resistanceTotal.IsChecked)
                symulation.Oblicz("resistance");
        }
    }
}
